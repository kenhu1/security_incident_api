Build a Security Incident API
=============================

Assignment Requirements:
https://gist.github.com/amissemer/8314efe851f979670f026c10161267f6

Build a Security Incident API
=============================

Assignment Requirements:
<https://gist.github.com/amissemer/8314efe851f979670f026c10161267f6>

Building Web API server
=======================

## Install Python3 and Flask

Python3 can be downloaded from <https://www.python.org/downloads/> and run the installer to install.
I'm using python 3.8.7

## Install Flask ##

Use the pip command to install Flask:

    $ pip install flask

## Start Web API server
To start the Web API server on your local laptop, cd to the directory where all the downloaded
files reside, and run the following command.

    $ python3 api_incidents.py


## Get Results from API Server
To get all types of incidents sorted by timestamp and grouped by employee and priority level for each employee,
go to the following URL in a browser

<http://localhost:9000/incidents>


You should see the following results:
```
    {
        647: {
            critical: {
                count: 1,
                incidents: [
                    {
                    machine_ip: "",
                    priority: "critical",
                    timestamp: "1624497078.44716",
                    type: "unauthorized"
                    }
                ]
            },
            high: {
                count: 0,
                incidents: []
            },
            low: {
                count: 0,
                incidents: []
            },
            medium: {
                count: 0,
                incidents: []
            }
        },
        735: {
            critical: {
                count: 1,
                incidents: [
                    {
                    machine_ip: "",
                    priority: "critical",
                    timestamp: "1624407109.7257",
                    type: "unauthorized"
                    }
                ]
                },
            high: {
                count: 0,
                incidents: [ ]
            },
            low: {
                count: 0,
                incidents: [ ]
                },
            medium: {
                count: 0,
                incidents: [ ]
            }
    },
    ...
```
# Issues found with the assignment#
- The mapping from `IP Address` to `employee_id` for all employees is incomplete. 
  - There is no match between the `IP Address` and `internal_ip` or `machine_ip` in the incidents data for intrusion, executable and probing types. As a result, there's no way to convert the `IP address` to `employee_id` for these data, thus the incidents data of these types can not be grouped by employee_id.
  - There's no match between the `employee_id` in the mapping data and the `employee_id` in denial, unauthorized and misuse type incident data (except only one match). As a result, no `machine_ips` are all empty all employees (except one) for these types of incidents.
- The required result data output has duplicated `priority level` field. The result is grouped by employee and priority, but priority again is listed in the `incidents` field.
- Suggested fix: the simplest fix is to make the mapping data complete.

# What's done #
- I've decided to preprocess the data and prepare the output in a JSON file to be loaded and returned by the API web server for the performance reason, since the API must return the results within 2 seconds. Processing data on in realtime or on the fly would require longer time.

# How data is processed #
- Raw incident data of all types (except other) are downloaded and extracted
- JSON data of all types are converted from JSON to CSV using `Pandas`, and loaded into the following the following tables in SQlite database file `incidents.db`:
  * denial
  * misuse
  * unauthorized
  * intrusion,
  * executable, 
  * probing
- Mapping data is loaded into table `mapping`
- Table 'incidents` only have merged data from denial, misuse and unauthorized. Type intrusion, executable and probing are not included because of missing employee_id.
- Run the following query in SQLite and output results to a file in CSV format:
```
    select employee_id, priority, type, machine_ip, timestamp
    from incidents
    group by 1, 2 order by 1, 2, timestamp;
```
- Using a simple Python program to convert the CSV file to the required JSON format.

# Files #
- `api_incidents.py`: Python Flask application for web server
- `incidents.json`: file to be read by the API web server that contains the incidents results grouped by employee and priority level.
- `translate.py`: a simple python scrip to conver CSV result to JSON
- `incidents.db`: a SQLite database file containing all the tables mentioned above.


