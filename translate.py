import json
import string

infile = 'incidents.csv'
outfile = 'incidents.json'

# incident data block fields: employee_id, priority, type, machine_ip, timestamp
priorities = ['low', 'medium', 'high','critical']

data = dict()
with open (infile, 'r') as file_in:
	for line in file_in:
		employee_id, priority, type, machine_ip, timestamp = line.rstrip().split(',')
		if employee_id not in data:
			data[employee_id] = {p:{"count": 0, "incidents": []} for p in priorities}
		incident = {"type": type, "priority": priority, "machine_ip": machine_ip, "timestamp": timestamp}
		data[employee_id][priority]["count"] += 1
		data[employee_id][priority]["incidents"].append(incident)

with open(outfile, 'w') as file_out:
	json.dump(data, file_out)


