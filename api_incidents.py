import flask
from flask import request, jsonify
import json

app = flask.Flask(__name__)
app.config["DEBUG"] = True


@app.route('/', methods=['GET'])
def home():
    return '''<h1>Hello</h1>
<p>A prototype API. Try http://localhost:9000/incidents</p>'''


@app.errorhandler(404)
def page_not_found(e):
    return "<h1>404</h1><p>The resource could not be found.</p>", 404


@app.route('/incidents', methods=['GET'])
def api_get_incidents():
    file = './incidents.json'
    with open(file, 'r') as fi:
        return json.load(fi)

app.run(host='0.0.0.0', port=9000)

